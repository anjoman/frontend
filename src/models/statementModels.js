/**
 * Created by nullmin on 6/4/17.
 */
import {BaseModel} from 'utils/models/base'
import {attr} from 'redux-orm'

class BaseStatment extends BaseModel {

}

BaseStatment.fields = {
    id: attr(),
    created: attr(),
    title: attr(),
    reason: attr(),
    endTime: attr(),
    signsNumber: attr()
};

export class StatementPreview extends BaseStatment {

}

StatementPreview.modelName = 'StatementPreview';
StatementPreview.fields = {
    ...BaseStatment.fields,
    thumbnailUrl: attr(),
    abstractContent: attr(),
};

export class Statement extends BaseStatment {

}

Statement.modelName = 'Statement';
Statement.fields = {
    ...BaseStatment.fields,
    imageUrl: attr(),
    content: attr(),
};


