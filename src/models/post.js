/**
 * Created by nullmin on 5/31/17.
 */
import {BaseModel} from "utils/models/base";
import {attr} from "redux-orm";

class BasePost extends BaseModel {

}

BasePost.fields = {
    created: attr(),
    title: attr(),
    tags: attr(),
    likesNumber: attr(),
};

export class Post extends BasePost {

}

Post.modelName = 'Post';

Post.fields = {
    ...BasePost.fields,
    imageUrl: attr(),
    content: attr(),
};

export class PostPreview extends BasePost {

}

PostPreview.modelName = 'PostPreview';

PostPreview.fields = {
    ...BasePost.fields,
    abstractContent: attr(),
    thumbnailUrl: attr(),
};
