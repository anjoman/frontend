/**
 * Created by nullmin on 6/4/17.
 */
import {modelArrayFetchAction, modelFetchAction} from "utils/models/actions";

export const fetchStatements = modelArrayFetchAction({
    endpoint: 'statement/statements/',
    modelName: 'StatementPreview'
});

export const fetchStatement = (id) => {
    return modelFetchAction({
        endpoint: 'statement/statements/' + id + '/',
        modelName: 'Statement'
    })();
};
