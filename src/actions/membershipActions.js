import {CALL_API} from "utils/constants";
import {SubmissionError} from "redux-form";
import {API_ACTIONS} from 'settings/settings'
import {createActionTypesFor} from 'utils/helpers'

export const applyMembership = (data) => (dispatch) => {
    let types = createActionTypesFor(API_ACTIONS.MEMBERSHIP);
    return dispatch({
        [CALL_API]: {
            types: [
                types.REQUEST, types.SUCCESS, types.FAILURE
            ],
            endpoint: 'membership/',
            method: 'post',
            body: data
        }
    }).catch(error => {
        throw new SubmissionError(error);
    });
};