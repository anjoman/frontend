import {CALL_API} from "utils/constants";
import {SubmissionError} from "redux-form";
import {API_ACTIONS, ACTIONS} from 'settings/settings'
import {createActionTypesFor} from 'utils/helpers'
import cookie from 'js-cookie'

export const join = (data) => (dispatch) => {
    let types = createActionTypesFor(API_ACTIONS.REGISTER);
    return dispatch({
        [CALL_API]: {
            types: [
                types.REQUEST, types.SUCCESS, types.FAILURE
            ],
            endpoint: 'join/',
            method: 'post',
            body: data
        }
    }).catch(error => {
        throw new SubmissionError(error);
    });
};

export const openLogin = () => (dispatch) => {
    return dispatch({type: ACTIONS.OPEN_LOGIN})
};

export const login = (data) => (dispatch) => {
    let types = createActionTypesFor(API_ACTIONS.LOGIN);
    return dispatch({
        [CALL_API]: {
            types: [
                types.REQUEST, types.SUCCESS, types.FAILURE
            ],
            endpoint: 'login/',
            method: 'post',
            body: data
        }
    }).then(response => {
        cookie.set('token', response.token);
        return Promise.resolve(response);
    }, error => {
        throw new SubmissionError(error);
    })
};

export const closeLogin = () => (dispatch) => {
    return dispatch({type: ACTIONS.CLOSE_LOGIN})
};

export const logout = () => (dispatch) => {
    cookie.remove('token');
    return dispatch({type: ACTIONS.LOGOUT})
};

export const getProfile = () => {
    let types = createActionTypesFor(API_ACTIONS.GET_PROFILE);
    return ({
        [CALL_API]: {
            types: [
                types.REQUEST, types.SUCCESS, types.FAILURE
            ],
            endpoint: 'profile/'
        }
    })
};

export const setProfile = (data) => (dispatch) => {
    let types = createActionTypesFor(API_ACTIONS.SET_PROFILE);
    return dispatch({
        [CALL_API]: {
            types: [
                types.REQUEST, types.SUCCESS, types.FAILURE
            ],
            endpoint: 'profile/',
            method: 'post',
            body: data
        }
    }).catch(error => {
        throw new SubmissionError(error);
    })
};