import cookie from 'js-cookie'
import {createActionTypesFor} from 'utils/helpers'
import {API_ACTIONS} from 'settings/settings'

let loginTypes = createActionTypesFor(API_ACTIONS.LOGIN);
export const start = () => (dispatch) => {
    let token = cookie.get('token');
    if (token) {
        return dispatch({
            type: loginTypes.SUCCESS,
            payload: {
                token: token
            }
        })
    }
};