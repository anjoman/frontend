/**
 * Created by nullmin on 5/31/17.
 */
import {modelArrayFetchAction, modelFetchAction} from 'utils/models/actions'
import {getNextFor} from 'selectors/paginateSelectors'

export const fetchPostCollectionFor = tag => (dispatch, getState) => {
    let endpoint = tag === 'all' ? 'blog/posts/' : 'blog/posts/' + tag + '/';
    console.log(getNextFor(tag)(getState()));
    return dispatch(modelArrayFetchAction({
        endpoint: getNextFor(tag)(getState()) || endpoint,
        modelName: 'PostPreview',
        paginateKey: tag
    })());
};

export const fetchPost = (id) => {
    return modelFetchAction({
        endpoint: 'blog/posts/' + id + '/',
        modelName: 'Post'
    })();
};