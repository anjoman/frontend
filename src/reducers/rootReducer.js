/**
 * Created by nullmin on 5/31/17.
 */
import {combineReducers} from "redux";
import {orm} from "utils/models/orm";
import {reducer as formReducer} from "redux-form";
import {createReducer} from 'redux-orm'
import {authReducer} from "reducers/authReducers";
import {createPaginateReducer, Paginate} from "utils/paginate/exports";

const ormReducer = createReducer(orm);
const paginate = new Paginate();
const tags = ['all', 'سیاسی', 'فرهنگی', 'صنفی', 'زنان', 'مطالعات', 'تشکیلات'];
paginate.register(...tags);
const paginateReducer = createPaginateReducer(paginate);

export const rootReducer = combineReducers({
    orm: ormReducer,
    form: formReducer,
    auth: authReducer,
    paginate: paginateReducer
});