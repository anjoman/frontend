/**
 * Created by nullmin on 6/2/17.
 */
import {createActionTypesFor} from "utils/helpers";
import {ACTIONS, API_ACTIONS} from "settings/settings";

let loginTypes = createActionTypesFor(API_ACTIONS.LOGIN);
let profileTypes = createActionTypesFor(API_ACTIONS.GET_PROFILE);
const initialSate = {
    showLoginModal: false,
    isLoggedIn: false,
    token: '',
    profile: {}
};
export const authReducer = (state = initialSate, action) => {
    switch (action.type) {
        case loginTypes.SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                token: action.payload.token
            };
        case ACTIONS.LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                token: ''
            };
        case ACTIONS.OPEN_LOGIN:
            return {
                ...state,
                showLoginModal: true
            };
        case ACTIONS.CLOSE_LOGIN:
            return {
                ...state,
                showLoginModal: false
            };
        case profileTypes.SUCCESS:
            return {
                ...state,
                profile: action.payload
            };
        default:
            return state;
    }
};