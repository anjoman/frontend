/**
 * Created by nullmin on 6/3/17.
 */

export const getAuthState = state => state.auth;
export const getOrmState = state => state.orm;
export const getPaginateState = state => state.paginate;