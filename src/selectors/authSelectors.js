/**
 * Created by nullmin on 6/3/17.
 */
import {getAuthState} from 'selectors/rootSelectors'

export const getLoginModal = (state) => {
    let authState = getAuthState(state);
    return authState.showLoginModal;
}

export const userIsLoggedIn = (state) => {
    let authState = getAuthState(state);
    return authState.isLoggedIn;
};

export const getToken = (state) => {
    let authState = getAuthState(state);
    return authState.token;
};

export const getProfile = (state) => {
    let authState = getAuthState(state);
    return authState.profile;
};