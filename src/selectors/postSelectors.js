/**
 * Created by nullmin on 6/4/17.
 */
import {createSelector} from 'redux-orm'
import {getOrmState} from 'selectors/rootSelectors'
import {orm} from 'utils/models/orm'

export const getPostsBy = tag => state => {
    return createSelector(orm, session => {
        let PostPreview = session['PostPreview'];
        return PostPreview.filter(post => {
            if (tag === 'all')
                return true;
            return post.tags.includes(tag);
        }).toRefArray();
    })(getOrmState(state));
};


