/**
 * Created by nullmin on 6/6/17.
 */
import {getPaginateState} from 'selectors/rootSelectors'
import {getNextFor as _getNextFor} from 'utils/paginate/exports'

export const getNextFor = (key) => (state) => {
    return _getNextFor(key)(getPaginateState(state));
};