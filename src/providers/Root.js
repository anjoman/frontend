/**
 * Created by nullmin on 5/31/17.
 */
import {Provider} from 'react-redux'
import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {App} from 'components/App'

export const Root = ({store, history}) => (
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
);
