/**
 * Created by nullmin on 6/4/17.
 */
import {getPostsBy} from 'selectors/postSelectors'
import {connect} from 'react-redux'

const mapStateToProps = (state, props) => {
    return {
        items: getPostsBy(props.tag)(state),
    }
};

export const postCollectionByTagProvider = connect(mapStateToProps, null);
