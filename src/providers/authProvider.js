/**
 * Created by nullmin on 6/2/17.
 */
import {connect} from 'react-redux'
import {userIsLoggedIn, getProfile, getLoginModal} from 'selectors/authSelectors'

const mapStateToProps = (state) => {
    return {showLoginModal: getLoginModal(state), isLoggedIn: userIsLoggedIn(state), profile: getProfile(state)}
};
export const authProvider = connect(mapStateToProps, null);
