import {PostCollectionPage} from 'components/post/postCollectionPage/PostCollectionPage';
import {PostPage} from 'components/post/PostPage';
import {StatementCollectionPage} from 'components/statement/StatementCollectionPage.js';
import {StatementPage} from 'components/statement/StatementPage';
import {ProfilePage} from 'components/auth/profile/ProfilePage';
import {LoginModal} from 'components/auth/login/LoginModal';
import {JoinPage} from 'components/auth/join/JoinPage';
import {MembershipPage} from 'components/membership/MembershipPage';
import {Route} from 'react-router-dom';
import React from 'react'

export class PrimaryContainer extends React.Component {
    render() {
        return (
            <div className="container primary">
                <Route exact name='login' component={LoginModal}/>
                <Route exact name='posts' path='/posts/:tag' component={PostCollectionPage}/>
                <Route exact name='post' path="/post/:id" component={PostPage}/>
                <Route exact name='join' path='/join' component={JoinPage}/>
                <Route exact name='membership' path='/membership' component={MembershipPage}/>
                <Route
                    exact
                    name="statements"
                    path='/statements'
                    component={StatementCollectionPage}/>
                <Route exact name='statement' path='/statement/:id' component={StatementPage}/>
                <Route exact name='profile' path='/profile' component={ProfilePage}/>
            </div>
        );
    }
}