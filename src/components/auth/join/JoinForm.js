import {Form, FormGroup, Button} from 'react-bootstrap';
import React from 'react'
import {reduxForm, Field} from 'redux-form'
import {renderInput} from 'utils/forms/renderInput'
import * as actions from 'actions/authActions'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

class JoinForm extends React.Component {
    onSubmit(datas) {
        return this
            .props
            .join(datas)
            .then(() => {
                this
                    .props
                    .history
                    .push('login');
            });
    }

    render() {
        return (
            <form
                onSubmit={this
                .props
                .handleSubmit(this.onSubmit.bind(this))}
                className="join">
                <Form componentClass="fieldset" horizontal>
                    <Field
                        name="name"
                        component={renderInput}
                        id="formControlsText"
                        type="text"
                        label="نام و نام خانوادگی"
                        placeholder="فرنود مسعودی"
                        inline="false"/>
                    <Field
                        name="student_number"
                        component={renderInput}
                        id="formControlsText"
                        type="text"
                        label="شماره دانشجویی"
                        placeholder="91110099"
                        inline="false"/>
                    <Field
                        name="email"
                        component={renderInput}
                        id="formControlsEmail"
                        type="email"
                        label="ایمیل"
                        placeholder="ferine.hellion@gmail.com"
                        inline="false"/>
                    <Field
                        name="password"
                        component={renderInput}
                        id="formControlsPassword"
                        type="password"
                        label="رمز عبور"
                        inline="false"/>
                    <FormGroup>
                        <Button type="submit" bsStyle="primary" disabled={this.props.submitting} block>
                            ثبت نام
                        </Button>
                    </FormGroup>
                </Form>
            </form>
        )
    }
}

JoinForm = connect(null, actions)(JoinForm);
JoinForm = reduxForm({form: 'join'})(JoinForm);
JoinForm = withRouter(JoinForm);
export {JoinForm};