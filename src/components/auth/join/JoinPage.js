import {JoinForm} from './JoinForm';
import {Route} from 'react-router-dom';
import React from 'react'

export class JoinPage extends React.Component {
    render() {
        return (
            <div className="join-container">
                <Route component={JoinForm}/>
            </div>
        )
    }
}