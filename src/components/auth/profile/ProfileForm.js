import {Form, FormGroup, Button} from 'react-bootstrap';
import React from "react";
import {Field, reduxForm} from "redux-form";
import {renderInput} from 'utils/forms/renderInput';
import {connect} from "react-redux";
import {setProfile} from "actions/authActions";

export class ProfileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMessage: false
        };
    }

    onSubmit(datas) {
        return this
            .props
            .setProfile(datas)
            .then(() => {
                this.setState({showMessage: true});
            });
    }

    render() {
        return (
            <form
                onSubmit={this
                .props
                .handleSubmit(this.onSubmit.bind(this))}
                className="profile">
                <Form componentClass="fieldset" horizontal>
                    <Field
                        name="name"
                        component={renderInput}
                        id="formControlsText"
                        type="text"
                        label="نام و نام خانوادگی"
                        inline="false"/>
                    <Field
                        name="email"
                        component={renderInput}
                        id="formControlsEmail"
                        type="email"
                        label="ایمیل"
                        inline="false"/>
                    <Field
                        name="new_password"
                        component={renderInput}
                        id="formControlsPassword"
                        type="password"
                        label="رمز عبور جدید"
                        inline="false"
                        help="در صورتی که میخوهید رمزتان را عوض کنید این قسمت را پر کنید"/>
                    <Field
                        name="password"
                        component={renderInput}
                        id="formControlsPassword"
                        type="password"
                        label="رمز عبور"
                        inline="false"
                        help="برای تغییر پروفایل وارد کردن رمز عبور الزامیست"/>
                    <Field
                        name="non_field_errors"
                        component={renderInput}
                        id="formControlsText"
                        type="hidden"/>
                    <FormGroup>
                        <Button type="submit" bsStyle="primary" disabled={this.props.submitting} block>
                            تغییر پروفایل
                        </Button>
                    </FormGroup>
                    {this.state.showMessage && <div>موفقیت آمیز تغییر کرد</div>}
                </Form>
            </form>
        )
    }
}
ProfileForm = reduxForm({form: 'profile'})(ProfileForm);
ProfileForm = connect(null, {setProfile})(ProfileForm);
