import {Route} from 'react-router-dom';
import React from 'react'
import {connect} from 'react-redux'
import {getProfile} from 'actions/authActions'
import {ProfileForm} from 'components/auth/profile/ProfileForm'
import {authProvider} from 'providers/authProvider'
import {initialize} from 'redux-form'

export class ProfilePage extends React.Component {
    componentDidMount() {
        this
            .props
            .getProfile()
            .then(() => {
                this
                    .props
                    .initialize('profile', this.props.profile);
            });
    }

    render() {
        return (
            <div className="profile-container">
                <Route component={ProfileForm}/>
            </div>
        );
    }
}

ProfilePage = connect(null, {getProfile, initialize})(ProfilePage);
ProfilePage = authProvider(ProfilePage);