import {LoginForm} from './LoginForm';
import {Route} from 'react-router-dom';
import React from 'react'

export class LoginModal extends React.Component {
    render() {
        return (
            <div className="static-modal login-container">
                <Route component={LoginForm}/>
            </div>
        )
    }
}