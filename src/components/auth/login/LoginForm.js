import {Button, Form, Modal} from 'react-bootstrap';
import React from "react";
import {Field, reduxForm} from "redux-form";
import {renderInput} from 'utils/forms/renderInput';
import {connect} from "react-redux";
import {login, closeLogin} from "actions/authActions";
import {withRouter} from 'react-router-dom'
import {authProvider} from 'providers/authProvider'

class LoginForm extends React.Component {
    onSubmit(data) {
        return this
            .props
            .login(data)
            .then(() => {
                this
                    .props
                    .closeLogin();
            })
    }

    render() {
        return (
            <Modal
                show={this.props.showLoginModal}
                onHide={this.props.closeLogin}
                animation={true}>
                <Modal.Header>
                    <Modal.Title>ورود به سایت انجمن اسلامی شریف</Modal.Title>
                </Modal.Header>
                <form
                    className="login"
                    onSubmit={this
                    .props
                    .handleSubmit(this.onSubmit.bind(this))}>
                    <Form componentClass="fieldset" horizontal>
                        <Modal.Body>
                            <Field
                                name="student_number"
                                component={renderInput}
                                id="formControlsText"
                                type="text"
                                label="شماره دانشجویی"
                                placeholder="91110099"/>
                            <Field
                                name="password"
                                component={renderInput}
                                id="formControlsPassword"
                                type="password"
                                label="رمز عبور"/>
                        </Modal.Body>
                        <Field
                            name="non_field_errors"
                            component={renderInput}
                            id="formControlsText"
                            type="hidden"/>
                        <Modal.Footer>
                            <Button bsStyle="link" onClick={this.props.closeLogin}>بستن</Button>
                            <Button type="submit" bsStyle="primary" disabled={this.props.submitting}>
                                ورود
                            </Button>
                        </Modal.Footer>
                    </Form>
                </form>
            </Modal>
        );
    }
}

LoginForm = authProvider(LoginForm);
LoginForm = reduxForm({form: 'login'})(LoginForm);
LoginForm = connect(null, {login, closeLogin})(LoginForm);
LoginForm = withRouter(LoginForm);
export {LoginForm}
