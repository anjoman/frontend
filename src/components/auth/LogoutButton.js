/**
 * Created by nullmin on 6/3/17.
 */
import React from 'react'
import {logout} from 'actions/authActions'
import {connect} from 'react-redux'

export class LogoutButton extends React.Component {
    render() {
        let {name} = this.props;
        return (
            <div onClick={this.props.logout}>{name}</div>
        );
    }
}

LogoutButton = connect(null, {logout})(LogoutButton);


