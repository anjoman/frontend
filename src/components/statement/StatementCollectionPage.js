import {Grid} from 'react-bootstrap';
import React from 'react'
import {connect} from 'react-redux'
import {fetchStatements} from 'actions/statementActions'
import {ModelListView} from 'utils/components/ListView'
import {AllProvider} from 'utils/models/providers'
import {StatementPreview} from 'components/statement/StatementPreview'

export class StatementCollectionPage extends React.Component {
    componentDidMount() {
        this
            .props
            .fetchStatements();
    }

    render() {
        return (
            <Grid className="statement-collection-container">
                <ModelListView
                    className="row fix"
                    items={this.props.items}
                    Component={StatementPreview}/>
            </Grid>
        )
    }
}

StatementCollectionPage = connect(null, {fetchStatements})(StatementCollectionPage);
StatementCollectionPage = AllProvider('StatementPreview')(StatementCollectionPage);