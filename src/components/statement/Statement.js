import React from "react";
import {ObjectProvider} from "utils/models/providers";
import {Media} from "react-bootstrap";
import defaultImage from "assets/images/default-post-image.jpg";

//todo feri joon khoda kheiret bede dige :))) in copy paste poste :)
class Statement extends React.Component {

    render() {
        let {obj} = this.props;
        let image = obj.imageUrl
            ? obj.imageUrl
            : defaultImage;
        return (
            <Media className="post-preview">
                <Media.Left align="top">
                    <img width={128} height={128} src={image} alt={obj.id}/>
                </Media.Left>
                <Media.Body>
                    <Media.Heading className="post-preview-title">{obj.title}</Media.Heading>
                    <p className="post-preview-content">{obj.content}</p>
                </Media.Body>
            </Media>
        )
    }
}

Statement = ObjectProvider({modelName: 'Statement'})(Statement);
export {Statement}