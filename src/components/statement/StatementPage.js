/**
 * Created by nullmin on 6/4/17.
 */
/**
 * Created by nullmin on 6/2/17.
 */
import React from 'react'
import {Statement} from 'components/statement/Statement'
import {fetchStatement} from 'actions/statementActions'
import {connect} from 'react-redux'
import {ObjectProvider} from 'utils/models/providers'

export class StatementPage extends React.Component {
    componentDidMount() {
        this.statementId = this.props.match.params.id;
        this.props.fetchStatement(this.statementId);
    }

    render() {
        if (this.props['statement'])
            return (<Statement id={this.statementId}/>);
        return (<div>...</div>)
    }
}

StatementPage = connect(null, {fetchStatement})(StatementPage);
StatementPage = ObjectProvider({
    modelName: 'Statement',
    viaRouter: true,
    objName: 'statement'
})(StatementPage);
