import {Link} from 'react-router-dom';
import {Col} from 'react-bootstrap';
import React from 'react'
import {ObjectProvider} from 'utils/models/providers'
import defaultImage from "assets/images/default-post-image.jpg";
import Time from 'react-time';

export class StatementPreview extends React.Component {
    render() {
        let {obj} = this.props;
        let image = obj.thumbnailUrl
            ? obj.thumbnailUrl
            : defaultImage;
        let endTime = new Date(obj.endTime)
        return (
            <Col xs={7} sm={5} md={3} className="statement-preview-column">
                <div className="thumbnail">
                    <img className="img-responsive full-width" src={image} alt={obj.id}/>
                    <div className="caption">
                        <h4 className="statement-preview-title">{obj.title}</h4>
                        <p className="statement-preview-date">
                            مهلت امضا تا&nbsp;
                            <Time value={endTime} format="MM/DD"/>
                            &nbsp;ساعت&nbsp;
                            <Time value={endTime} format="HH:mm"/>
                        </p>
                        <p className="statement-preview-content">{obj.abstractContent}&nbsp;...</p>
                    </div>
                    <div className="row statement-preview-buttons">
                        <Col xs={8} sm={6} md={6}>
                            <Link to={'/statement/' + obj.id} className="without-color">
                                <button className="btn btn-primary btn-block">متن کامل</button>
                            </Link>
                        </Col>
                        <Col xs={8} sm={6} md={6}>
                            <Link to={'/statement/sign/' + obj.id} className="without-color">
                                <button className="btn btn-danger outline btn-block">امضا می‌کنم</button>
                            </Link>
                        </Col>
                    </div>
                </div>
            </Col>
        )
    }
}

StatementPreview = ObjectProvider({modelName: 'StatementPreview'})(StatementPreview);