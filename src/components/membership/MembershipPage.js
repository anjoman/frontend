import {MEMBERSHIP_CONTENT} from 'settings/settings'
import {MembershipForm} from './MembershipForm';
import {Route} from 'react-router-dom';
import React from 'react'

// TODO: MEMBERSHIP_CONTENT
export class MembershipPage extends React.Component {
    render() {
        return (
            <div className="membership-container">
                {/*<div>
                    <p>
                        {MEMBERSHIP_CONTENT}
                    </p>
                </div>*/}
                <Route component={MembershipForm}/>
            </div>
        )
    }
}