import {ANJOMAN_UNITS, GRADES, DEPARTMENTS} from 'settings/settings';
import {
    Button,
    Form,
    Col,
    FormGroup,
    Grid,
    Row
} from 'react-bootstrap';
import React from 'react'
import {reduxForm, Field} from 'redux-form'
import * as actions from 'actions/membershipActions'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {renderInput, renderCheckboxGroup} from 'utils/forms/renderInput'

class MembershipForm extends React.Component {
    onSubmit(datas) {
        return this
            .props
            .applyMembership(datas)
            .then(() => {
                this
                    .props
                    .history
                    .push('/');
            });
    }

    render() {
        let gradeItems = GRADES.map(item => {
            return (
                <option key={item} value={item}>{item}</option>
            );
        });
        let departmentItems = DEPARTMENTS.map(item => {
            return (
                <option key={item} value={item}>{item}</option>
            );
        });
        return (
            <form
                onSubmit={this
                .props
                .handleSubmit(this.onSubmit.bind(this))}
                className="membership">
                <Grid>
                    <Row>
                        <Col md={6}>
                            <Form componentClass="fieldset" horizontal>
                                <Field
                                    name="student_number"
                                    component={renderInput}
                                    id="formControlsText"
                                    type="text"
                                    label="شماره دانشجویی"
                                    placeholder="91110099"/>
                                <Field
                                    name="first_name"
                                    component={renderInput}
                                    id="formControlsText"
                                    type="text"
                                    label="نام"
                                    placeholder="فرنود"/>
                                <Field
                                    name="last_name"
                                    component={renderInput}
                                    id="formControlsText"
                                    type="text"
                                    label="نام خانوادگی"
                                    placeholder="مسعودی"/>
                                <Field
                                    name="email"
                                    component={renderInput}
                                    id="formControlsEmail"
                                    type="email"
                                    label="ایمیل"
                                    placeholder="ferine.hellion@gmail.com"/>
                                <Field
                                    name="phone_number"
                                    component={renderInput}
                                    id="formControlsText"
                                    type="text"
                                    label="شماره تماس"
                                    placeholder="09127016105"/>
                                <Field
                                    name="related_experiences"
                                    component={renderInput}
                                    id="formControlsTextarea"
                                    label="تجارب پیشین"
                                    componentClass="textarea"
                                    placeholder="هر رزومه و تجربه‌ی مشابه‌ای که داشتید یا دارید را در اینجا برای ما بگویید"/>
                            </Form>
                        </Col>
                        <Col md={6}>
                            <Form componentClass="fieldset" horizontal>
                                <Field
                                    name="grade"
                                    component={renderInput}
                                    id="formControlsSelect"
                                    label="مقطع تحصیلی"
                                    select={gradeItems}
                                    componentClass="select"
                                    placeholder="مقطع تحصیلی خود را انتخاب کنید"/>
                                <Field
                                    name="department"
                                    component={renderInput}
                                    id="formControlsSelect"
                                    label="دانشکده"
                                    select={departmentItems}
                                    componentClass="select"
                                    placeholder="دانشکده خود را انتخاب کنید"/>
                                <Field
                                    name="units_of_interest"
                                    component={renderCheckboxGroup}
                                    label="واحدهای مورد علاقه"
                                    help="می‌توانید بیش از یک واحد انتخاب کنید"
                                    options={ANJOMAN_UNITS}/>
                                <FormGroup>
                                    <Col smOffset={2} sm={10}>
                                        <Button type="submit" bsStyle="primary" disabled={this.props.submitting} block>
                                            درخواست عضویت
                                        </Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Grid>
            </form>
        )
    }
}

MembershipForm = connect(null, actions)(MembershipForm);
MembershipForm = reduxForm({form: 'membership'})(MembershipForm);
MembershipForm = withRouter(MembershipForm);
export {MembershipForm};