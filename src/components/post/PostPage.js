/**
 * Created by nullmin on 6/2/17.
 */
import React from 'react'
import {Post} from 'components/post/Post.js'
import {fetchPost} from 'actions/post'
import {connect} from 'react-redux'
import {ObjectProvider} from 'utils/models/providers'

export class PostPage extends React.Component {
    componentDidMount() {
        this.postId = this.props.match.params.id;
        this.props.fetchPost(this.postId);
    }

    render() {
        if (this.props['post'])
            return (<Post id={this.postId}/>);
        return (<div>...</div>)
    }
}

PostPage = connect(null, {fetchPost})(PostPage);
PostPage = ObjectProvider({modelName: 'Post', viaRouter: true, objName: 'post'})(PostPage);
