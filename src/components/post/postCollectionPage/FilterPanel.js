/**
 * Created by nullmin on 6/3/17.
 */
import React from "react";
import {LinkContainer} from "react-router-bootstrap";
import {ListGroupItem, ListGroup} from "react-bootstrap";

export class FilterPanel extends React.Component {
    render() {
        let {items} = this.props;
        let listItems = items.map(item => {
            return (
                <LinkContainer to={item.href} key={item.id}>
                    <ListGroupItem>
                        <p className="post-collection-list-filter-item">{item.name}</p>
                    </ListGroupItem>
                </LinkContainer>
            );
        });
        return (
            <ListGroup>
                {listItems}
            </ListGroup>
        )
    }
}