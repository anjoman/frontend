import {Col, Row, Button} from "react-bootstrap";
import React from "react";
import {fetchPostCollectionFor} from "actions/post";
import {connect} from "react-redux";
import {FilterPanel} from "components/post/postCollectionPage/FilterPanel";
import {PostCollectionPanel} from "components/post/postCollectionPage/PostCollectionPanel";
import {withRouter} from "react-router-dom";

let listFilters = [
    {
        'id': 1,
        'name': 'همه مطالب',
        'href': '/posts/all'
    }, {
        'id': 2,
        'name': 'سیاسی'
    }, {
        'id': 3,
        'name': 'فرهنگی'
    }, {
        'id': 4,
        'name': 'صنفی'
    }, {
        'id': 5,
        'name': 'زنان'
    }, {
        'id': 6,
        'name': 'مطالعات'
    }, {
        'id': 7,
        'name': 'تشکیلات'
    }
];

listFilters = listFilters.map(item => {
    return item['href']
        ? item
        : {
            ...item,
            href: '/posts/' + item.name
        }

});

class PostCollectionPage extends React.Component {

    //todo nemitooni khodet hads bezani moshkel chiye??? :|
    fetchPosts() {
        this
            .props
            .fetchPostCollectionFor(this.props.match.params.tag);
    }

    componentDidMount() {
        this.fetchPosts();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.tag !== this.props.match.params.tag) 
            this.fetchPosts()
    }

    //todo in stylasho doros kon masoudi :))
    render() {
        return (
            <Row className="post-collection-container">
                <Col md={3}>
                    <FilterPanel items={listFilters}/>
                </Col>
                <Col md={9}>
                    <PostCollectionPanel tag={this.props.match.params.tag}/>
                    <Button
                        bsStyle="primary"
                        className="statement-preview-more-button gradient"
                        onClick={this
                        .fetchPosts
                        .bind(this)}
                        block>بعدی</Button>
                </Col>

            </Row>
        );
    }
}

PostCollectionPage = connect(null, {fetchPostCollectionFor})(PostCollectionPage);
PostCollectionPage = withRouter(PostCollectionPage);
export {PostCollectionPage}