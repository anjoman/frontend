/**
 * Created by nullmin on 6/4/17.
 */
import React from "react";
import {postCollectionByTagProvider} from 'providers/postProviders'
import {ModelListView} from 'utils/components/ListView'
import {PostPreview} from 'components/post/PostPreview'

export class PostCollectionPanel extends React.Component {
    render() {
        let {items} = this.props;
        return (
            <ModelListView items={items} Component={PostPreview}/>
        )
    }
}

PostCollectionPanel = postCollectionByTagProvider(PostCollectionPanel);