import {LinkContainer} from 'react-router-bootstrap';
import {Button, Media} from 'react-bootstrap';
import React from 'react'
import {ObjectProvider} from 'utils/models/providers'
import defaultImage from 'assets/images/default-post-image.jpg'
import Time from 'react-time'

class PostPreview extends React.Component {
    render() {
        let {obj} = this.props;
        let image = obj.thumbnailUrl
            ? obj.thumbnailUrl
            : defaultImage;
        let created = new Date(obj.created)
        return (
            <Media className="post-preview">
                <Media.Left align="top">
                    <img className="post-preview-image" src={image} alt={obj.id}/>
                </Media.Left>
                <Media.Body className="post-preview-body">
                    <Media.Heading className="post-preview-title">{obj.title}</Media.Heading>
                    <p className="post-preview-date">
                        نوشته شده در&nbsp;
                        <Time value={created} format="MM/DD"/>
                        &nbsp;ساعت&nbsp;
                        <Time value={created} format="HH:mm"/>
                    </p>
                    <p className="post-preview-content">{obj.abstractContent}&nbsp;...</p>
                    <LinkContainer to={'/post/' + obj.id}>
                        <Button bsSize="large" block>ادامه</Button>
                    </LinkContainer>
                </Media.Body>
            </Media>
        )
    }
}

PostPreview = ObjectProvider({modelName: 'PostPreview'})(PostPreview);
export {PostPreview};