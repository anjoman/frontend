import {ICONS} from 'settings/settings';
import React from 'react';

export class Footer extends React.Component {
    render() {
        let iconItems = ICONS.map(icon => {
            return (
                <li key={icon.id}>
                    <a href={icon.href} target="_blank" rel="noopener noreferrer">
                        <i className={icon.className}></i>
                    </a>
                </li>
            );
        });
        return (
            <footer className="footer">
                <div className="container">
                    <p className="pull-right">تمامی حقوق مادی و معنوی این سایت متعلق به انجمن اسلامی دانشجویان شریف است.</p>
                    <ul className="footer-icons pull-left">
                        {iconItems}
                    </ul>
                </div>
            </footer>
        );
    }
}