import {SERVICES} from 'settings/settings';
import {Link} from 'react-router-dom';
import React from 'react';

export class Services extends React.Component {
    render() {
        let serviceItems = SERVICES.map(service => {
            return (
                <Link to={service.href} key={service.id} className="service-href">
                    <div className="col-lg-3 col-md-6 text-center">
                        <div className="service-box">
                            <i className={"fa fa-4x " + service.className + " sr-icons"}></i>
                            <h3>{service.title}</h3>
                            <p className="lead">{service.body}</p>
                        </div>
                    </div>
                </Link>
            );
        });
        return (
            <section id="services">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <hr className="light"/>
                            <hr className="light"/>
                            <h1 className="section-heading">{"خدمات سایت انجمن"}</h1>
                            <hr className="primary"/>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {serviceItems}
                    </div>
                </div>
                <hr className="light"/>
                <hr className="light"/>
            </section>
        );
    }
}