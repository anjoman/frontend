import {ICONS} from 'settings/settings';
import React from 'react';

export class Banner extends React.Component {
    render() {
        let iconItems = ICONS.map((icon, index) => {
            if (index >= 3) {
                return null;
            }
            return (
                <li key={icon.id}>
                    <a
                        href={icon.href}
                        className="btn btn-default btn-lg"
                        target="_blank"
                        rel="noopener noreferrer">
                        <i className={icon.className + " fa-fw"}></i>&nbsp;
                        <span className="network-name">{icon.name}</span>
                    </a>
                </li>
            );
        });
        return (
            <div className="banner">
                <div className="container">
                    <div className="row">
                        <div className="banner-col col-lg-6">
                            <h2>{"با انجمن در ارتباط باشید :"}</h2>
                        </div>
                        <div className="col-lg-6">
                            <ul className="list-inline banner-social-buttons">
                                {iconItems}
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}