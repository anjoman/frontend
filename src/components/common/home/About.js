import React from 'react';

export class About extends React.Component {
    render() {
        return (
            <section className="bg-primary" id="about">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-lg-offset-2 text-center">
                            <hr className="bg-primary"/>
                            <hr className="bg-primary"/>
                            <h1 className="section-heading">{"درباره ما"}</h1>
                            <hr className="light"/>
                            <p className="text-faded lead">{"چقدر انجمن باحاله!"}</p>
                            <hr className="bg-primary"/>
                            <hr className="bg-primary"/>
                            <hr className="bg-primary"/>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}