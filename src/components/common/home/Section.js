import React from 'react';

export const Section = ({imageSrc, header1, header2, body, leftImage}) => {
    return (
        <div
            className={leftImage
            ? "content-section-a"
            : "content-section-b"}>
            <div className="container">
                <div className="row">
                    <div
                        className={leftImage
                        ? "col-lg-5 col-sm-6"
                        : "col-lg-5 col-lg-offset-1 col-sm-push-6 col-sm-6"}>
                        <hr className="section-heading-spacer"/>
                        <div className="clearfix"></div>
                        <h2 className="section-heading">{header1}<br/>{header2}</h2>
                        <p className="lead">{body}</p>
                    </div>
                    <div
                        className={leftImage
                        ? "col-lg-5 col-lg-offset-2 col-sm-6"
                        : "col-lg-5 col-sm-pull-6 col-sm-6"}>
                        <img className="img-responsive" src={imageSrc} alt={header1}/>
                    </div>
                </div>
            </div>
        </div>
    );
};