import {Services} from './Services';
import {About} from './About';
import {SectionCollection} from './SectionCollection';
import {Banner} from './Banner';
import {Header} from './Header';
import {Route} from 'react-router-dom';
import React from 'react';

export class HomePage extends React.Component {
    render() {
        return (
            <div className="home">
                <Route component={Header}/>
                <Route component={About}/>
                <Route component={Services}/>
                <Route component={SectionCollection}/>
                <Route component={Banner}/>
            </div>
        );
    }
}