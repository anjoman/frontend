import {Section} from './Section';
import React from 'react';
import image from 'assets/images/intro-bg.jpg'

export class SectionCollection extends React.Component {
    render() {
        return (
            <div className="sections">
                <Section
                    imageSrc={image}
                    header1="دنیای تیره و تار"
                    header2="برای ما آمادست"
                    body="یه چیزی میخوام بگم ولی نگین چرا داری میگی!"
                    leftImage={true}/>
                <Section
                    imageSrc={image}
                    header1="دنیای تیره و تار"
                    header2="برای ما آمادست"
                    body="یه چیزی میخوام بگم ولی نگین چرا داری میگی!"
                    leftImage={false}/>
                <Section
                    imageSrc={image}
                    header1="دنیای تیره و تار"
                    header2="برای ما آمادست"
                    body="یه چیزی میخوام بگم ولی نگین چرا داری میگی!"
                    leftImage={true}/>
                <Section
                    imageSrc={image}
                    header1="دنیای تیره و تار"
                    header2="برای ما آمادست"
                    body="یه چیزی میخوام بگم ولی نگین چرا داری میگی!"
                    leftImage={false}/>
            </div>
        );
    }
}