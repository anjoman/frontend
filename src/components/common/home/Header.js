import {ICONS} from 'settings/settings';
import React from 'react';

export class Header extends React.Component {
    render() {
        let iconItems = ICONS.map((icon, index) => {
            if (index >= 3) {
                return null;
            }
            return (
                <li key={icon.id}>
                    <a
                        href={icon.href}
                        className="btn btn-default btn-lg"
                        target="_blank"
                        rel="noopener noreferrer">
                        <i className={icon.className + " fa-fw"}></i>&nbsp;
                        <span className="network-name">{icon.name}</span>
                    </a>
                </li>
            );
        });
        return (
            <div className="intro-header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="intro-message">
                                <h1>{process.env.REACT_APP_WEBSITE_NAME}</h1>
                                <h3>{"سایتی برای تعامل دانشجویان با انجمن و دانشگاه صنعتی شریف"}</h3>
                                <hr className="intro-divider"/>
                                <ul className="list-inline intro-social-buttons">
                                    {iconItems}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}