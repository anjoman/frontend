import {IndexLinkContainer} from 'react-router-bootstrap';
import {Nav, NavItem} from 'react-bootstrap';
import {openLogin} from 'actions/authActions'
import {connect} from 'react-redux'
import React from 'react';

export class LoggedOutNavbar extends React.Component {
    render() {
        return (
            <Nav pullLeft>
                <IndexLinkContainer to='/join'>
                    <NavItem eventKey={6}>ثبت نام</NavItem>
                </IndexLinkContainer>
                <NavItem eventKey={5} onClick={this.props.openLogin}>ورود</NavItem>
            </Nav>
        );
    }
}

LoggedOutNavbar = connect(null, {openLogin})(LoggedOutNavbar);