import {IndexLinkContainer} from 'react-router-bootstrap';
import {Nav, NavItem} from 'react-bootstrap';
import React from 'react';
import {LogoutButton} from 'components/auth/LogoutButton'
import {withRouter} from 'react-router-dom'

export class LoggedInNavbar extends React.Component {
    logout() {
        return this.props.logout().then(() => {
            this.props.history.push('login');
        })
    }

    render() {
        return (
            <Nav pullLeft>
                <IndexLinkContainer to='/profile'>
                    <NavItem eventKey={5}>پروفایل</NavItem>
                </IndexLinkContainer>
                <IndexLinkContainer to='login'>
                    <NavItem eventKey={6}><LogoutButton name="خروج"/></NavItem>
                </IndexLinkContainer>
            </Nav>
        );
    }
}

LoggedInNavbar = withRouter(LoggedInNavbar);