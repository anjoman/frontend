import {SERVICES} from 'settings/settings';
import {LoggedOutNavbar} from './LoggedOutNavbar';
import {LoggedInNavbar} from './LoggedInNavbar';
import {Route} from 'react-router-dom';
import {authProvider} from 'providers/authProvider'
import {IndexLinkContainer} from 'react-router-bootstrap';
import {Nav, Navbar as BootNavbar, NavItem} from 'react-bootstrap';
import React from 'react';

export class Navbar extends React.Component {
    render() {
        let leftNavbar = null;
        if (this.props.isLoggedIn) {
            leftNavbar = <Route component={LoggedInNavbar}/>;
        } else {
            leftNavbar = <Route component={LoggedOutNavbar}/>;
        }
        let rightNavItems = SERVICES.map(item => {
            return (
                <IndexLinkContainer to={item.href} key={item.id}>
                    <NavItem eventKey={item.id}>{item.title}</NavItem>
                </IndexLinkContainer>
            );
        });
        return (
            <BootNavbar inverse collapseOnSelect fixedTop fluid>
                <BootNavbar.Header>
                    <BootNavbar.Brand>
                        <IndexLinkContainer to='/'>
                            <a>{process.env.REACT_APP_WEBSITE_NAME}</a>
                        </IndexLinkContainer>
                    </BootNavbar.Brand>
                    <BootNavbar.Toggle/>
                </BootNavbar.Header>
                <BootNavbar.Collapse>
                    <Nav>
                        {rightNavItems}
                    </Nav>
                    {leftNavbar}
                </BootNavbar.Collapse>
            </BootNavbar>
        );
    }
}

Navbar = authProvider(Navbar)