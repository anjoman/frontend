import {Footer} from 'components/common/Footer';
import {Navbar} from 'components/common/navbar/Navbar';
import {HomePage} from 'components/common/home/HomePage';
import {PrimaryContainer} from 'components/PrimaryContainer';
import {Route, Switch} from 'react-router-dom';
import React from 'react';

// TODO: blank page for everything + not available page and error page
export class App extends React.Component {
    render() {
        return (
            <div className="App">
                <div className="wrapper">
                    <Route component={Navbar}/>
                    <Switch>
                        <Route exact name='home' path='/' component={HomePage}/>
                        <Route name='container' path='/*' component={PrimaryContainer}/>
                    </Switch>
                    <div className="push"></div>
                </div>
                <Route component={Footer}/>
            </div>
        );
    }
}