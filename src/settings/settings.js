import {Post, PostPreview} from 'models/post'
import {Statement, StatementPreview} from 'models/statementModels'

export const API_ROOT = process.env.REACT_APP_API_ROOT;
export const ENTITIES = [Post, PostPreview, Statement, StatementPreview];
export const COLLECTIONS = [];
export const API_ACTIONS = {
    REGISTER: 'REGISTER',
    LOGIN: 'LOGIN',
    MEMBERSHIP: 'MEMBERSHIP',
    GET_PROFILE: 'GET_PROFILE',
    SET_PROFILE: 'SET_PROFILE'
};

export const ACTIONS = {
    LOGOUT: 'LOGOUT',
    OPEN_LOGIN: 'OPEN_LOGIN',
    CLOSE_LOGIN: 'CLOSE_LOGIN'
};

// TODO: correct links
export const ICONS = [
    {
        'id': 1,
        'name': 'توییتر',
        'className': 'fa fa-twitter',
        'href': 'https://twitter.com/anjoman'
    }, {
        'id': 2,
        'name': 'اینستاگرام',
        'className': 'fa fa-instagram',
        'href': 'https://instagram.com/anjoman'
    }, {
        'id': 3,
        'name': 'تلگرام',
        'className': 'fa fa-telegram',
        'href': 'https://telegram.com/anjoman'
    }, {
        'id': 4,
        'name': 'گیتلب',
        'className': 'fa fa-gitlab',
        'href': 'https://gitlab.com/anjoman'
    }
];
export const SERVICES = [
    {
        'id': 1,
        'className': 'fa-newspaper-o',
        'title': 'اخبار',
        'href': '/posts/all',
        'body': 'در این قسمت مقالات و اخباری که توسط دانشجویان نوشته می‌شود را بخوانید و لذت ببری' +
                'د'
    }, {
        'id': 2,
        'className': 'fa-paper-plane',
        'title': 'بیانیه‌ها',
        'href': '/statements',
        'body': 'در اینجا می‌توانید بیانیه‌های انجمن را دیده و درصورت تمابل امضا کنید تا اثر بیشت' +
                'ری در دانشگاه بگذاریم'
    }, {
        'id': 3,
        'className': 'fa-diamond',
        'title': 'دیدگاه‌ها',
        'href': '/opinions',
        'body': 'دیدگاه‌ها، نظرات، نقد و پیشنهادات خود را به انجمن و یا دانشگاه به صورت ناشناس نو' +
                'شته و همچنین دیدگاه‌های برتر را نگاه کنید'
    }, {
        'id': 4,
        'className': 'fa-heart',
        'title': 'عضویت در انجمن',
        'href': '/membership',
        'body': 'در صورت تمایل می‌توانید فرم عضویت انجمن را پر کرده و به عضویت رسمی انجمن درآیید.' +
                ' توجه داشته باشید که ثبت نام در سایت به معنی عضویت انجمن نیست و صرفا در جهت تسهی' +
                'ل خدمات است.'
    }
]
export const ANJOMAN_UNITS = [
    'سیاسی',
    'فرهنگی',
    'صنفی',
    'زنان',
    'مطالعات',
    'تشکیلات'
];
export const GRADES = ['کارشناسی', 'کارشناسی ارشد', 'دکترا']
export const DEPARTMENTS = [
    'دانشکده شیمی',
    'دانشکده علوم ریاضی',
    'دانشکده فیزیک',
    'دانشکده مدیریت و اقتصاد',
    'دانشکده مهندسی انرژی',
    'دانشکده مهندسی برق',
    'دانشکده مهندسی شیمی و نفت',
    'دانشکده مهندسی صنایع',
    'دانشکده مهندسی عمران',
    'دانشکده مهندسی کامپیوتر',
    'دانشکده مهندسی مکانیک',
    'دانشکده مهندسی و علم مواد',
    'دانشکده مهندسی هوافضا'
]

export const MEMBERSHIP_CONTENT = "چرت و پرت"
