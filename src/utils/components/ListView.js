import React from 'react'

export class ModelListView extends React.Component {
    render() {
        let {
            items,
            className,
            Component,
            idPropName = 'id'
        } = this.props;
        let representation = items.map(item => {
            return <Component {...{key: item.id, [idPropName]: item.id}}/>
        });
        return (
            <div className={className}>{representation}</div>
        );
    }
}