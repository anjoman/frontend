/**
 * Created by nullmin on 6/6/17.
 */

export const _getNextFor = (key) => (state) => {
    return state[key].next;
};
