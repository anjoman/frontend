/**
 * Created by nullmin on 6/6/17.
 */
import {SET_NEXT} from './constants'

export const createReducer = (paginate) => {
    return (state = getInitialState(paginate), action) => {
        switch (action.type) {
            case SET_NEXT:
                return {
                    ...state,
                    [action.payload.key]: {next: action.payload.next}
                };
            default:
                return state;
        }

    }
};

const getInitialState = (paginate) => {
    let result = {};
    paginate.registry.forEach(key => {
        result[key] = {
            next: '',
        }
    });
    return result;
};

