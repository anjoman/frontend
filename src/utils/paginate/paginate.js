/**
 * Created by nullmin on 6/6/17.
 */
export class Paginate {
    constructor() {
        this.registry = [];
    }

    register(...keys) {
        keys.forEach(key => {
            this.registry.push(key);
        })
    }
}