/**
 * Created by nullmin on 6/6/17.
 */
import {SET_NEXT} from './constants'

export const setNext = (paginateKey, next) => {
    return {
        type: SET_NEXT,
        payload: {
            key: paginateKey,
            next: next
        }
    }
};