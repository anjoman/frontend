/**
 * Created by nullmin on 5/31/17.
 */
import {orm} from 'utils/models/orm'


export class BaseModelManager {
    constructor(modelName) {
        this.modelName = modelName;
        this.Model = orm.get(modelName);
    }

    getManager(state) {
        let session = orm.session(state);
        console.log(session);
        return session[this.modelName];
    }

    get(state, id) {
        let manager = this.getManager(state);
        if (manager.hasId(id))
            return manager.withId(id).ref;
        return 'asghar';
    }
}