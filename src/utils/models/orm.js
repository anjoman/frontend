/**
 * Created by nullmin on 5/31/17.
 */

import {ORM} from "redux-orm";
import {ENTITIES, COLLECTIONS} from "settings/settings";

const orm = new ORM();

orm.register(...ENTITIES, ...COLLECTIONS);

for (let Model of ENTITIES) {
    Model.setUp(orm);
}

for (let Collection of COLLECTIONS) {
    Collection.setUp(orm);
}

export {orm}
