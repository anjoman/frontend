/**
 * Created by nullmin on 5/31/17.
 */
import {Model, attr} from "redux-orm";
import {objectIterator} from "utils/helpers";
import {Schema, arrayOf} from "normalizr";
import {FETCH} from 'utils/constants'

class Base extends Model {

    static setUp(orm) {
    }

    static getTypesFor(mode) {
        return {
            REQUEST: mode + "_" + this.modelName + "_REQUEST",
            SUCCESS: mode + "_" + this.modelName + "_SUCCESS",
            FAILURE: mode + "_" + this.modelName + "_FAILURE",
        };
    }
}

class BaseModel extends Base {

    static createUpdate(instance) {
        if (this.hasId(instance.id))
            this.withId(instance.id).update(instance);
        else
            this.create(instance);
    }

    static reducer(action, ConcreteModel) {
        if (action.hasOwnProperty("entities")) {
            if (action.entities.hasOwnProperty(ConcreteModel.modelName)) {
                let itemsById = action.entities[ConcreteModel.modelName];
                Object.keys(itemsById).forEach(id => {
                    ConcreteModel.createUpdate(itemsById[id]);
                });
            }
        }
    }

    static setUp(orm) {
        this.createSchema(orm);
        //todo again circular error!
        // this.createManager();
        this.setDefaultIdAttribute();
    }

    static createSchema(orm) {
        let idAttribute = this.options.idAttribute || "id";
        let schema = new Schema(this.modelName, {idAttribute: idAttribute});
        let relations = {};
        for (let [name, relation] of objectIterator(this.fields)) {
            if (relation.hasOwnProperty("toModelName")) {
                relations[name] = orm.get(relation.toModelName).schema;
            }
        }
        schema.define(relations);
        this.schema = schema;
        this.arraySchema = arrayOf(schema);
    }


    static setDefaultIdAttribute() {
        if (!this.options.idAttribute)
            this.options['idAttribute'] = 'id'
    }


}

BaseModel.schema = null;
BaseModel.arraySchema = null;
BaseModel.objects = null;

class BaseCollection extends Base {

    static reducer(action, Collection) {
        switch (action.type) {
            case this.getTypesFor(FETCH).SUCCESS:
                action.result.forEach(id => {
                    Collection.create({id: id});
                });
                break;
            default:
        }
    }
}

BaseCollection.targetModel = null;
BaseCollection.fields = {
    id: attr(),
};
export {BaseModel, BaseCollection}
