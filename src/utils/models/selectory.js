/**
 * Created by nullmin on 5/31/17.
 */

import {createSelector} from "redux-orm";
import {orm} from "utils/models/orm";

export const selectORM = (state) => state.orm;

export const createGetSelector = (modelName, id) => {
    return createSelector(orm, session => {
        let Model = session[modelName];
        if (Model.hasId(id))
            return session[modelName].withId(id).ref;
        return null;
    });
};

export const createAllSelector = (modelName) => {
    return createSelector(orm, session => {
        return session[modelName].all().toModelArray(obj => {
            return obj.ref;
        });
    });
};

export const createInverseSelector = (modelName, relatedName, id) => {
    return createSelector(orm, session => {
        return session[modelName].withId(id)[relatedName].toRefArray();
    });
};

export const createCollectionSelector = (collectionName) => {
    return createSelector(orm, session => {
        return session[collectionName].all().toRefArray();
    });
};