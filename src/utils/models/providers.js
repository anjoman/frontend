/**
 * Created by nullmin on 5/31/17.
 */
import {orm} from 'utils/models/orm'
import {connect} from "react-redux";
import {
    createGetSelector,
    createAllSelector,
    createCollectionSelector,
    createInverseSelector,
    selectORM,
} from "utils/models/selectory";
import {convertFirstToLower} from "utils/helpers";

/**
 *  The component must have the idAttribute of the Model as a Props
 */
const ObjectProvider = ({modelName, idPropName, objName = 'obj', viaRouter = false}) => {
    idPropName = idPropName ? idPropName : orm.get(modelName).options.idAttribute;
    return connect((state, props) => {
        let id = viaRouter ? props.match.params[idPropName] : props[idPropName];
        return {
            [objName]: createGetSelector(modelName, id)(selectORM(state)),
        };
    }, {});
};

const CollectionProvider = (collectionName, propName = 'items') => {
    return connect((state) => {
        return {
            [propName]: createCollectionSelector(collectionName)(selectORM(state)),
        };
    });
};

const AllProvider = (modelName, propName = 'items') => {
    return connect((state) => {
        return {
            [propName]: createAllSelector(modelName)(selectORM(state)),
        };
    });
};

/**
 * The Component props must have modelNameId;
 */
const InverseProvider = (modelName, relatedName) => {
    return connect((state, props) => {
        let id = props[convertFirstToLower(modelName) + "Id"];
        return {
            [relatedName]: createInverseSelector(modelName, relatedName, id)(selectORM(state)),
        };
    });
};

export {ObjectProvider, CollectionProvider, AllProvider, InverseProvider};
