/**
 * Created by nullmin on 5/31/17.
 */
import {CALL_API} from "utils/constants";
import {orm} from "utils/models/orm";
import {CREATE, FETCH} from "utils/constants";

export const modelCreateAction = ({endpoint, modelName, data = {}, payload}) => () => {
    let Model = orm.get(modelName);
    return ({
        [CALL_API]: {
            types: Object.values(Model.getTypesFor(CREATE)),
            endpoint,
            method: "post",
            body: data,
            schema: Model.schema,
            mode: CREATE,
        },
        payload: payload,
    });
};

export const modelArrayFetchAction = ({endpoint, modelName, payload, paginateKey}) => () => {
    let Model = orm.get(modelName);
    return ({
        [CALL_API]: {
            types: Object.values(Model.getTypesFor(FETCH)),
            endpoint,
            schema: Model.arraySchema,
            paginateKey
        },
        payload: payload,
    });
};

export const modelFetchAction = ({endpoint, modelName, payload}) => () => {
    let Model = orm.get(modelName);
    return ({
        [CALL_API]: {
            types: Object.values(Model.getTypesFor(FETCH)),
            endpoint,
            schema: Model.schema,
        },
        payload: payload,
    });
};

export const collectionFetchAction = ({endpoint, collectionName, payload}) => () => {
    let Collection = orm.get(collectionName);
    let TargetModel = orm.get(Collection.targetModel);
    return ({
        [CALL_API]: {
            types: Object.values(Collection.getTypesFor(FETCH)),
            endpoint,
            schema: TargetModel.arraySchema,
        },
        payload: payload,
    });
};

