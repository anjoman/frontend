import {normalize} from "normalizr";
import {camelizeKeys} from "humps";
import {API_ROOT} from "settings/settings";
import {CALL_API} from 'utils/constants'
import cookie from 'js-cookie'
import {setNext} from 'utils/paginate/exports'

export const callApi = (endpoint, method = "get", body, schema = null) => {
    //todo token must be get from state;
    const token = cookie.get('token');
    const fullUrl = (endpoint.indexOf(API_ROOT) === -1)
        ? API_ROOT + endpoint
        : endpoint;
    let headers = {
        "Content-Type": "application/json",
        "authorization": ""
    };
    if (token)
        headers.authorization = "JWT " + token;
    return fetch(fullUrl, {
        method,
        body: JSON.stringify(body),
        headers
    }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response
                    .json()
                    .then(json => {
                        let result = schema
                            ? {
                                ...normalize(camelizeKeys((json.results || json)), schema),
                                next: json.next
                            }
                            : json;
                        return Promise.resolve(result);
                    },);
            } else if (response.status === 400 || response.status === 401)
                return response.json().then(json => Promise.reject(json));
        }
        , () => {
            console.error("check the server please.");
            return Promise.reject({networkMessage: "run the server!"});
        });

};

export const api = store => next => action => {
    const callApiParams = action[CALL_API];
    if (typeof callApiParams === "undefined")
        return next(action);
    let {endpoint, types, method, body, schema, paginateKey} = callApiParams;
    if (typeof endpoint !== "string") {
        throw new Error("Specify a string endpoint URL.");
    }
    const [requestType,
        successType,
        failureType] = types;
    const actionWith = data => {
        const resultAction = Object.assign({}, action, data);
        delete resultAction[CALL_API];
        return resultAction;
    };
    next(actionWith({type: requestType}));
    return callApi(endpoint, method, body, schema).then(response => {
        if (response.next && paginateKey)
            store.dispatch(setNext(paginateKey, response.next));
        if (schema)
            next(actionWith({
                type: successType,
                entities: response.entities,
                result: response.result
            }));
        else
            next(actionWith({type: successType, payload: response}));
        return Promise.resolve(response);
    }, error => {
        next(actionWith({error, type: failureType}));
        return Promise.reject(error);
    },);

};
