/**
 * Created by nullmin on 5/31/17.
 */
import React from 'react'

export const objectIterator = (obj) => {
    return Object.keys(obj).map(key => [key, obj[key]]);
};


export const convertFirstToLower = (s = "") => {
    return s.charAt(0).toLowerCase() + s.slice(1);
};


export const renderField = ({input, label, type, meta: {touched, error, warning}}) => {
    return (
        <div>
            <label>{label}</label>
            <div>
                <input {...input} placeholder={label} type={type}/>
                {touched && ((error && <span>{error}</span>) || (warning &&
                <span>{warning}</span>))}
            </div>
        </div>
    );
};


/**
 * for example you get REGISTER and then you get [REGISTER_REQUEST,...]
 */
export const createActionTypesFor = (actionType) => {
    return {
        REQUEST: actionType + '_REQUEST',
        SUCCESS: actionType + '_SUCCESS',
        FAILURE: actionType + '_FAILURE',
    }
};
