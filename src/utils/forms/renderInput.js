import {FormGroup, ControlLabel, FormControl, HelpBlock, Col} from 'react-bootstrap';
import React from 'react';

const getErrors = (error, warning, touched) => {
    let message = null;
    const validationState = (touched && ((error && "error") || (warning && "warning") || "success")) || null;

    if (touched && (error || warning)) {
        message = <HelpBlock>{error || warning}</HelpBlock>;
    }

    return [validationState, message];
}

const getResult = (id, label, customInput, feedbackIcon, help, errorMessages, inline = true) => {
    if (inline === "false") {
        return (
            <FormGroup validationState={errorMessages[0]} controlId={id}>
                <ControlLabel>{label}</ControlLabel>
                {customInput}
                {feedbackIcon
                    ? <FormControl.Feedback>{feedbackIcon}</FormControl.Feedback>
                    : errorMessages[1]
                        ? <FormControl.Feedback/>
                        : null}
                {help && <HelpBlock>{help}</HelpBlock>}
                {errorMessages[1]}
            </FormGroup>
        );
    }
    return (
        <FormGroup validationState={errorMessages[0]} controlId={id}>
            <Col componentClass={ControlLabel} sm={2}>{label}</Col>
            <Col sm={10}>
                {customInput}
                {feedbackIcon
                    ? <FormControl.Feedback>{feedbackIcon}</FormControl.Feedback>
                    : errorMessages[1]
                        ? <FormControl.Feedback/>
                        : null}
                {help && <HelpBlock>{help}</HelpBlock>}
                {errorMessages[1]}
            </Col>
        </FormGroup>
    );
}

export const renderInput = ({
    input,
    type,
    id,
    label,
    select,
    feedbackIcon,
    help,
    inline,
    meta: {
        error,
        warning,
        touched
    },
    ...props
}) => {
    const errorMessages = getErrors(error, warning, touched);
    const customInput = <FormControl {...input} type={type} {...props}>{select}</FormControl>
    return getResult(id, label, customInput, feedbackIcon, help, errorMessages, inline);
};

export const renderCheckboxGroup = ({
    input,
    id,
    label,
    options,
    feedbackIcon,
    help,
    meta: {
        error,
        warning,
        touched
    }
}) => {
    const errorMessages = getErrors(error, warning, touched);
    const customInput = options.map((option, index) => (
        <div className="checkbox" key={index}>
            <label>
                <input
                    type="checkbox"
                    name={option}
                    value={option}
                    checked={input
                    .value
                    .indexOf(option) !== -1}
                    onChange={event => {
                    const newValue = [...input.value];
                    if (event.target.checked) {
                        newValue.push(option);
                    } else {
                        newValue.splice(newValue.indexOf(option), 1);
                    }
                    return input.onChange(newValue);
                }}/> {option}
            </label>
        </div>
    ));
    return getResult(id, label, customInput, feedbackIcon, help, errorMessages);
};