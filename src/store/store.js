import {rootReducer} from "reducers/rootReducer";
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {api} from "utils/middlewares/api";
import createLogger from "redux-logger";

export const configureStore = (initialState) => {
    return createStore(rootReducer, initialState, applyMiddleware(thunk, api, createLogger));
};
