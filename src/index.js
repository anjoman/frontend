import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import "index.css";

import React from "react";
import ReactDOM from "react-dom";
import {Root} from "providers/Root";
import registerServiceWorker from "registerServiceWorker";
import {configureStore} from "store/store";
import {start} from "actions/starterActions";

document.title = process.env.REACT_APP_WEBSITE_NAME;

const store = configureStore();
store.dispatch(start());
ReactDOM.render(
    <Root store={store}/>, document.getElementById('root'));
registerServiceWorker();
